<?php
namespace Skipper\Versions;

use Skipper\Versions\Contracts\Versionable;

trait HasVersion
{
    /**
     * @var string|null
     */
    private $hash;

    /**
     * @var int
     */
    private $versionId = 1;

    /**
     * @param int $version
     * @return Versionable
     */
    public function setCurrentVersion(int $version): Versionable
    {
        $this->versionId = $version;

        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentVersion(): int
    {
        return $this->versionId;
    }

    /**
     * @param string $hash
     * @return Versionable
     */
    public function setContentHash(string $hash): Versionable
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getContentHash(): ?string
    {
        return $this->hash;
    }
}