<?php
namespace Skipper\Versions\Exceptions;

use Skipper\Exceptions\Error;
use Throwable;

class VersionNotFoundException extends VersionException
{
    public function __construct(
        string $location,
        array $context = [],
        Throwable $previous = null,
        int $code = 0
    ) {
        parent::__construct('Version not found', $location, $context, $previous, $code);

        $this->errors = [];
        $this->addError(new Error('Version not found', 'notFound', $location));
    }
}