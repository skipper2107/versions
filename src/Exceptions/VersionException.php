<?php
namespace Skipper\Versions\Exceptions;

use Skipper\Exceptions\Error;
use Skipper\Repository\Exceptions\RepositoryException;
use Throwable;

class VersionException extends RepositoryException
{
    public function __construct(
        string $message,
        string $location,
        array $context = [],
        Throwable $previous = null,
        int $code = 0
    ) {
        parent::__construct($message, $location, $context, $previous, $code);

        $this->errors = [];
        $this->addError(new Error($message, 'versionError', $location));
    }

    /**
     * @param RepositoryException $e
     * @return VersionException
     */
    public static function fromRepositoryException(RepositoryException $e): VersionException
    {
        $ve = new static($e->getMessage(), '', $e->getData(), $e, $e->getCode());

        $ve->errors = [];
        foreach ($e->getErrors() as $error) {
            $ve->addError($error);
        }

        return $ve;
    }
}