<?php
namespace Skipper\Versions;

use Skipper\Repository\Contracts\Repository;
use Skipper\Repository\Exceptions\StorageException;
use Skipper\Versions\Contracts\Versionable;
use Skipper\Versions\Contracts\VersionableRepository;
use Skipper\Versions\Exceptions\VersionException;
use Skipper\Versions\Exceptions\VersionNotFoundException;

class VersionManager
{
    /**
     * @param Versionable $entity
     * @param int $version
     * @param VersionableRepository $versions
     * @param Repository $mainEntityStorage
     * @return Versionable
     * @throws VersionException
     * @throws VersionNotFoundException
     */
    public function revertToVersion(
        Versionable $entity,
        int $version,
        VersionableRepository $versions,
        Repository $mainEntityStorage
    ): Versionable {
        $specificVersion = $versions->getSpecificVersion($entity, $version);

        $this->writeVersion($specificVersion, $versions, $mainEntityStorage);

        return $specificVersion;
    }

    /**
     * @param Versionable $entity
     * @param VersionableRepository $versions
     * @param Repository $mainStorage
     * @throws VersionException
     */
    public function writeVersion(Versionable $entity, VersionableRepository $versions, Repository $mainStorage): void
    {
        $originalHash = $entity->getContentHash();
        $newHash = $this->calculateContentHash($entity, $versions);
        if ($originalHash === $newHash) {
            return;
        }
        $entity->setCurrentVersion($entity->getCurrentVersion() + 1)
            ->setContentHash($newHash);

        try {
            $mainStorage->save($entity);
            $versions->save($entity);
        } catch (StorageException $e) {
            throw VersionNotFoundException::fromRepositoryException($e);
        }
    }

    /**
     * @param Versionable $entity
     * @param VersionableRepository $versions
     * @return string
     */
    public function calculateContentHash(Versionable $entity, VersionableRepository $versions): string
    {
        $versionableData = $versions->getVersionableData($entity);
        $representation = var_export($versionableData, true);

        return md5($representation);
    }
}