<?php
namespace Skipper\Versions\Contracts;

use Skipper\Repository\Contracts\Entity;

interface Versionable extends Entity
{
    /**
     * @param int $version
     * @return Versionable
     */
    public function setCurrentVersion(int $version): Versionable;

    /**
     * @return int
     */
    public function getCurrentVersion(): int;

    /**
     * @param string $hash
     * @return Versionable
     */
    public function setContentHash(string $hash): Versionable;

    /**
     * @return null|string
     */
    public function getContentHash(): ?string;
}