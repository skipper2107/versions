<?php
namespace Skipper\Versions\Contracts;

use Skipper\Repository\Contracts\Repository;
use Skipper\Versions\Exceptions\VersionNotFoundException;

interface VersionableRepository extends Repository
{
    /**
     * @param Versionable $entity
     * @return array
     */
    public function getVersionableData(Versionable $entity): array;

    /**
     * @param Versionable $entity
     * @return Versionable[]
     */
    public function getAllVersionsOfEntity(Versionable $entity): array;

    /**
     * @param Versionable $entity
     * @param int $version
     * @throws VersionNotFoundException
     * @return Versionable
     */
    public function getSpecificVersion(Versionable $entity, int $version): Versionable;
}