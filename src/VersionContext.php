<?php
namespace Skipper\Versions;

use Skipper\Repository\Contracts\Repository;
use Skipper\Versions\Contracts\Versionable;
use Skipper\Versions\Contracts\VersionableRepository;

class VersionContext
{
    /**
     * @var VersionManager
     */
    private $manager;

    /**
     * @var VersionableRepository
     */
    private $versions;

    /**
     * @var Repository
     */
    private $storage;

    public function __construct(VersionManager $manager, VersionableRepository $versions, Repository $storage)
    {
        $this->manager = $manager;
        $this->versions = $versions;
        $this->storage = $storage;
    }

    /**
     * @param Versionable $entity
     * @throws Exceptions\VersionException
     */
    public function write(Versionable $entity): void
    {
        $this->manager->writeVersion($entity, $this->versions, $this->storage);
    }

    /**
     * @param Versionable $entity
     * @param int $version
     * @throws Exceptions\VersionException
     * @throws Exceptions\VersionNotFoundException
     */
    public function revert(Versionable $entity, int $version): void
    {
        $this->manager->revertToVersion($entity, $version, $this->versions, $this->storage);
    }

    /**
     * @param Versionable $entity
     * @return string
     */
    public function getContentHash(Versionable $entity): string
    {
        return $this->manager->calculateContentHash($entity, $this->versions);
    }
}